
require('dotenv').config();

module.exports = {
    server: {
        name: process.env.SERVER_NAME,
        version: process.env.SERVER_VERSION,
        port: process.env.SERVER_PORT
    },
    db: {
        uri: 'mongodb://localhost:27017' || process.env.DB_URI,
        name: process.env.DB_NAME || 'xsispro'
    }
}