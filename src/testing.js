
const { server } = ctx;

const MongoClient = require('mongodb').MongoClient;
const config = require('../config/config');

module.exports = () => {

    MongoClient.connect(config.db.uri, { useNewUrlParser: true }, (err, client) => {
        if(err) throw 'Message: ' + err;

        const db = client.db(config.db.name);

        const collection = db.collection('categories');

        server.route({
            method: 'GET',
            path: '/api/testing',
            auth: true,
            async handler(req, res) {
    
                collection.find({}).toArray().then(result => {
                    res.send(200,result);
                }).catch(err => {
                    throw 'Message: '+ + err;
                })
    
            }
        })
    })
    
}