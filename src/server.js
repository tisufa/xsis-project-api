const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware');

const MongoClient = require('mongodb').MongoClient;

const App = require('./common/Application');

global.files = require('./common/Files');

const config = require('../config/config');

const server = restify.createServer({
    name: config.server.name,
    version: config.server.version
});

/**
 * restify cors middleware
 */
const cors = corsMiddleware({
    origins: ['*'],
    allowHeaders: ['*'],
    exposeHeaders: ['*']
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.pre(cors.preflight);
server.use(cors.actual);

server.listen(config.server.port, () => {

    global.ctx = Object.assign({}, { server });

    App.start();
    require('./testing')();

})