
const Routes = require('./routes');
const RoleModel = require('./model/model');
const Role = require('./controller/controller');

module.exports = {
    name: 'Roles',
    start: async (server) => {

        Object.assign(ctx, { Role, RoleModel });
        server.route(Routes);
    }
}
