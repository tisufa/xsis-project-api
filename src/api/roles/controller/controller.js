
/**
 * Create Roles
 * 
 * @param {*} entity = req.body
 * @return {Promise}
 */
async function create(entity) {
    const { RoleModel } = ctx;

    const roles = await RoleModel.create(entity);

    return roles;
}

/**
 * Get All Roles
 * 
 * @param {*} query = {}
 * @return {Promise}
 */
async function getAll(query) {
    const { RoleModel } = ctx;

    const roles = await RoleModel.getAll(query);

    return roles;
}

/**
 * Get One Roles
 * 
 * @param {*} query = { _id: req.params.id }
 * @return {Promise}
 */
async function getOne(query) {
    const { RoleModel } = ctx;

    const role = await RoleModel.getOne(query);

    if (!role) {
        return new errors.NotFoundError('Role Not Found!!!');
    }

    return role;
}

/**
 * Update Roles
 * 
 * @param {*} query = { _id: req.params.id}
 * @param {*} entity = req.body
 * @return {Promise}
 */
async function update(query, entity) {
    const { RoleModel } = ctx;

    const isExists = await RoleModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Role Not Found!!!');
    }

    const role = await RoleModel.update(query, entity);

    return role;
}

/**
 * Delete Roles
 * 
 * @param {*} query = { _id: req.params.id }
 * @return {Promise}
 */
async function remove(query) {
    const { RoleModel } = ctx;

    const isExists = await RoleModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Role Not Found!!!');
    }

    const role = await RoleModel.remove(query);

    if (role) {
        return { data: 'Role has been deleted!' };
    }
}


module.exports = {
    create,
    getAll,
    getOne,
    update,
    remove
}