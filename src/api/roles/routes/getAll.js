
exports.route = {
    method: 'GET',
    path: '/api/roles',
    auth: true,
    async handler(req, res) {
        const { Role } = ctx;
        const roles = await Role.getAll({});
        res.send(200, roles);
    }
}