
exports.route = {
    method: 'POST',
    path: '/api/role',
    auth: true,
    async handler(req, res) {
        const { Role } = ctx;
        const entity = req.body;
        const role = await Role.create(entity);
        res.send(200, role);
    }
}