
exports.route = {
    method: 'DEL',
    path: '/api/role/:id',
    auth: true,
    async handler(req, res) {
        const { Role } = ctx;
        const query = { _id: req.params.id }
        const role = await Role.remove(query);
        res.send(200, role);
    }
}