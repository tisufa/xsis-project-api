
exports.route = {
    method: 'PUT',
    path: '/api/role/:id',
    auth: true,
    async handler(req, res) {
        const { Role } = ctx;
        const query = { _id: req.params.id };
        const entity = req.body;
        const role = await Role.update(query, entity);
        res.send(200, role);
    }
}