
/**
 * Create Hardskill
 * 
 * @param {*} entity  = req.body
 * @returns {Promise}
 */
async function create(entity) {
    const { HardskillModel } = ctx;

    const hardskill = await HardskillModel.create(entity);

    return hardskill;
}

/**
 * Get All Hardskill with entity 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function getAll(query) {
    const { HardskillModel } = ctx;

    const hardskills = await HardskillModel.getAll(query);

    return hardskills;
}

/**
 * Get One Hardskill with entity
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function getOne(query) {
    const { HardskillModel } = ctx;

    const hardskill = await HardskillModel.getOne(query);

    if (!hardskill) {
        return new errors.NotFoundError('Hardskill Not Found!!!');
    }

    return hardskill;
}

/**
 * Update Hardskill 
 * 
 * @param {*} query = { _id: req.params.id}
 * @param {*} entity = req.body
 * @returns {Promise}
 */
async function update(query, entity) {
    const { HardskillModel } = ctx;

    const isExists = await HardskillModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Hardskill Not Found!!!');
    }

    const hardskill = await HardskillModel.update(query, entity);

    return hardskill;
}

/**
 * Delete Batch
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function remove(query) {
    const { HardskillModel } = ctx;

    const isExists = await HardskillModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Hardskill Not Found!!!');
    }

    const hardskill = await HardskillModel.remove(query);

    if (hardskill) {
        return { data: 'Hardskill has been deleted successfully' };
    }
}

module.exports = {
    create,
    getAll,
    getOne,
    update,
    remove
}