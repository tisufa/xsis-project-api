
exports.route = {
    method: 'DEL',
    path: '/api/hardskill/:id',
    auth: true,
    async handler(req, res) {
        const { Hardskill } = ctx;
        const query = { _id: req.params.id }
        const hardskill = await Hardskill.remove(query);
        res.send(200, hardskill);
    }
}