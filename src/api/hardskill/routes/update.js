
exports.route = {
    method: 'PUT',
    path: '/api/hardskill/:id',
    auth: true,
    async handler(req, res) {
        const { Hardskill } = ctx;
        const query = { _id: req.params.id };
        const entity = req.body;
        const hardskill = await Hardskill.update(query, entity);
        res.send(200, hardskill);
    }
}