
exports.route = {
    method: 'GET',
    path: '/api/hardskill/:id',
    auth: true,
    async handler(req, res) {
        const { Hardskill } = ctx;
        const query = { _id: req.params.id };
        const hardskill = await Hardskill.getOne(query);
        res.send(200, hardskill);
    }
}