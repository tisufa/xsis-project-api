
exports.route = {
    method: 'GET',
    path: '/api/hardskill',
    auth: true,
    async handler(req, res) {
        const { Hardskill } = ctx;
        const hardskill = await Hardskill.getAll({})
        res.send(200, hardskill);
    }
}