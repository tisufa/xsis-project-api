
exports.route = {
    method: 'POST',
    path: '/api/hardskill',
    auth: true,
    async handler(req, res) {
        const { Hardskill } = ctx;
        const entity = req.body;
        const hardskill = await Hardskill.create(entity);
        res.send(200, hardskill);
    }
}