
const Routes = require('./routes');
const HardskillModel = require('./model/model');
const Hardskill = require('./controller/controller');

module.exports = {
    name: 'Hardskill',
    start: async (server) => {

        Object.assign(ctx, { Hardskill, HardskillModel });
        server.route(Routes);
    }
}
