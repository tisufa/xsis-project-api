
exports.route = {
    method: 'POST',
    path: '/api/menu',
    auth: true,
    async handler(req, res) {
        const { Menu } = ctx;
        const entity = req.body;
        const menu = await Menu.create(entity);
        res.send(200, menu);
    }
}