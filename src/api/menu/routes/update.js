
exports.route = {
    method: 'PUT',
    path: '/api/menu/:id',
    auth: true,
    async handler(req, res) {
        const { Menu } = ctx;
        const query = { _id: req.params.id };
        const entity = req.body;
        const menu = await Menu.update(query, entity);
        res.send(200, menu);
    }
}