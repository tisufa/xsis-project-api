
exports.route = {
    method: 'DEL',
    path: '/api/menu/:id',
    auth: true,
    async handler(req, res) {
        const { Menu } = ctx;
        const query = { _id: req.params.id }
        const menu = await Menu.remove(query);
        res.send(200, menu);
    }
}