
exports.route = {
    method: 'GET',
    path: '/api/menu',
    auth: true,
    async handler(req, res) {
        const { Menu } = ctx;
        const menu = await Menu.getAll({});
        res.send(200, menu);
    }
}