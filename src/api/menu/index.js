
const Routes = require('./routes');
const MenuModel = require('./model/model');
const Menu = require('./controller/controller');

module.exports = {
    name: 'Menu',
    start: async (server) => {
        
        Object.assign(ctx, { Menu, MenuModel });
        server.route(Routes);
    }
}
