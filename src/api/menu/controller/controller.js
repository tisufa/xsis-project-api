
/**
 * Create Menu
 * 
 * @param {*} entity = req.body
 * @return {Promise}
 */
async function create(entity) {
    const { MenuModel } = ctx;

    const menu = await MenuModel.create(entity);

    return menu;
}

/**
 * Get All Menu
 * @param {*} query = {}
 * @return {Promise}
 */
async function getAll(query) {
    const { MenuModel } = ctx;

    const menu = await MenuModel.getAll(query);

    return menu;
}

/**
 * Get One Menu
 * 
 * @param {*} query = { _id: req.params.id }
 * @return {Promise}
 */
async function getOne(query) {
    const { MenuModel } = ctx;

    const menu = await MenuModel.getOne(query);

    if (!menu) {
        return new errors.NotFoundError('Menu Not Found!!!');
    }

    return menu;
}

/**
 * Update menu
 * 
 * @param {*} query = { _id: req.params.id }
 * @param {*} entity =  req.body 
 * @return {Promise}
 */
async function update(query, entity) {
    const { MenuModel } = ctx;

    const isExists = await MenuModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Menu Not Found!!!');
    }

    const menu = await MenuModel.update(query, entity);

    return menu;
}

/**
 * Delete Menu 
 * 
 * @param {*} query = { _id: req.params.id }
 * @return {Promise}
 */
async function remove(query) {
    const { MenuModel } = ctx;

    const isExists = await MenuModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Menu Not Found!!!');
    }

    const menu = await MenuModel.remove(query);

    if (menu) {
        return { data: 'Menu has been deleted!' };
    }
}

module.exports = {
    create,
    getAll,
    getOne,
    update,
    remove
}