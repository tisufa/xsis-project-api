
exports.route = {
    method: 'POST',
    path: '/api/batch',
    auth: true,
    async handler(req, res) {
        const { Batch } = ctx;
        const entity = req.body;
        const batch = await Batch.create(entity);
        res.send(200, batch);
    }
}