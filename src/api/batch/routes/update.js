
exports.route = {
    method: 'PUT',
    path: '/api/batch/:id',
    auth: true,
    async handler(req, res) {
        const { Batch } = ctx;
        const query = { _id: req.params.id };
        const entity = req.body;
        const batch = await Batch.update(query, entity);
        res.send(200, batch);
    }
}