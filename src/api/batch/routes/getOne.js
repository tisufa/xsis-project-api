
exports.route = {
    method: 'GET',
    path: '/api/batch/:id',
    auth: true,
    async handler(req, response) {
        const { Batch } = ctx;
        const query = { _id: req.params.id };
        const batch = await Batch.getOne(query);
        res.send(200, batch);
    }
}