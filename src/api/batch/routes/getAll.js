
exports.route = {
    method: 'GET',
    path: '/api/batch',
    auth: true,
    async handler(req, res) {
        const { Batch } = ctx;

        const batch = await Batch.getAll({});
        res.send(200, batch);
    }
}