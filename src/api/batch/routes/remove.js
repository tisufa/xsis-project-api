
exports.route = {
    method: 'DEL',
    path: '/api/batch/:id',
    auth: true,
    async handler(req, res) {
        const { Batch } = ctx;
        const query = { _id: req.params.id }
        const batch = await Batch.remove(query);
        res.send(200, batch);
    }
}