
/**
 * Create Batch
 * 
 * @param {*} entity  = req.body
 * @returns {Promise}
 */
async function create(entity) {
    const { BatchModel } = ctx;

    const batch = await BatchModel.create(entity);

    return batch;

}

/**
 * Get All Technology with entity 
 * @param {*} query = {}
 * @returns {Promise}
 */
async function getAll(query) {
    const { BatchModel } = ctx;

    const batch = await BatchModel.getAll(query);

    return batch;
}

/**
 * Get One Batch with entity
 * 
 * @param {*} entity = { _id: req.params.id }
 * @returns {Promise}
 */
async function getOne(query) {
    const { BatchModel } = ctx;

    const batch = await BatchModel.getOne(query);

    if (!batch) {
        return new errors.NotFoundError('Batch Not Found!!!');
    }

    return batch;
}

/**
 * Update Batch 
 * 
 * @param {*} query = { _id: req.params.id}
 * @param {*} entity = req.body
 * @returns {Promise}
 */
async function update(query, entity) {
    const { BatchModel } = ctx;

    const isExists = await BatchModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Batch Not Found!!!');
    }

    const batch = await BatchModel.update(query, entity);

    return batch;
}

/**
 * Delete Batch
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function remove(query) {
    const { BatchModel } = ctx;

    const isExists = await BatchModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Batch Not Found!!!');
    }

    const batch = await BatchModel.remove(query);

    if (batch) {
        return { data: 'Batch has been deleted successfully' };
    }

}

module.exports = {
    create,
    getAll,
    getOne,
    update,
    remove
}