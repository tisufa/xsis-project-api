
const Routes = require('./routes');
const BatchModel = require('./model/model');
const Batch = require('./controller/controller');

module.exports = {
    name: 'Batch',
    start: async (server) => {

        Object.assign(ctx, { Batch, BatchModel });
        server.route(Routes);
    }
}
