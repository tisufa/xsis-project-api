
const Model = require(files.toCommon('Model'));

class MenuAccessModel extends Model {
    constructor(collection) {
        super(collection);
    }

    async getAll() {
        const collection = await this.collection;
        return collection.aggregate([
            { $lookup: { from: "roles", localField: "id_role", foreignField: "_id", as: "roleDoc" } },
            { $lookup: { from: "menu", localField: "id_menu", foreignField: "_id", as: "menuDoc" } },
            { $unwind: "$roleDoc" },
            { $unwind: "$menuDoc" },
            {
                $project: {
                    "roleId": "$id_role",
                    "roleName": "$roleDoc.name",
                    "menuId": "$id_menu",
                    "menuName": "$menuDoc.name",
                    "menuController": "$menuDoc.controller",
                }
            }
        ]).toArray().then(menuaccess => {
            return menuaccess;
        }).catch(err => {
            throw 'Message: ' + err;
        })
    }
}

module.exports = new MenuAccessModel('menu_access');