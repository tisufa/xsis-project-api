
exports.route = {
    method: 'GET',
    path: '/api/menuaccess/:id',
    auth: true,
    async handler(req, res) {
        const { MenuAccess } = ctx;
        const query = { _id: req.params.id };
        const menuaccess = await MenuAccess.getOne(query);
        res.send(200, menuaccess);
    }
}