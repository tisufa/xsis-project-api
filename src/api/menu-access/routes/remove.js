
exports.route = {
    method: 'DEL',
    path: '/api/menuaccess/:id',
    auth: true,
    async handler(req, res) {
        const { MenuAccess } = ctx;
        const query = { _id: req.params.id }
        const menuaccess = await MenuAccess.remove(query);
        res.send(200, menuaccess);
    }
}