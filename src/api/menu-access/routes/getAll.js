
exports.route = {
    method: 'GET',
    path: '/api/menuaccess',
    auth: true,
    async handler(req, res) {
        const { MenuAccess } = ctx;
        const menuaccess = await MenuAccess.getAll({});
        res.send(200, menuaccess);
    }
}