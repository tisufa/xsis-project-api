
exports.route = {
    method: 'POST',
    path: '/api/menuaccess',
    auth: true,
    async handler(req, res) {
        const { MenuAccess } = ctx;
        const entity = req.body;
        const menuaccess = await MenuAccess.create(entity);
        res.send(200, menuaccess);
    }
}