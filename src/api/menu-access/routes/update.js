
exports.route = {
    method: 'PUT',
    path: '/api/menuaccess/:id',
    auth: true,
    async handler(req, res) {
        const { MenuAccess } = ctx;
        const query = { _id: req.params.id };
        const entity = req.body;
        const menuaccess = await MenuAccess.update(query, entity);
        res.send(200, menuaccess);
    }
}