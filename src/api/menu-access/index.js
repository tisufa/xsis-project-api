
const Routes = require('./routes');
const MenuAccessModel = require('./model/model');
const MenuAccess = require('./controller/controller');

module.exports = {
    name: 'Menu Access',
    start: (server) => {

        Object.assign(ctx, { MenuAccess, MenuAccessModel });
        server.route(Routes);
    }
}