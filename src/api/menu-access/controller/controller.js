
/**
 * Create Menu Access
 * 
 * @param {*} entity = req.body
 * @returns {Promise}
 */
async function create(entity) {
    const { MenuAccessModel } = ctx;

    const menuaccess = await MenuAccessModel.create(entity);

    return menuaccess;
}

/**
 * Get All Menu Access
 * 
 * @param {*} query = {}
 * @returns {Promise}
 */
async function getAll(query) {
    const { MenuAccessModel } = ctx;

    const menuaccess = await MenuAccessModel.getAll(query);

    return menuaccess;
}

/**
 * Get One Menu Access
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function getOne(query) {
    const { MenuAccessModel } = ctx;

    const menuaccess = await MenuAccessModel.getOne(query);

    if (!menuaccess) {
        return new errors.NotFoundError('Menu Access Not Found!!!');
    }

    return menuaccess;
}

/**
 * Update Menu Access
 * 
 * @param {*} query = { _id: req.params.id }
 * @param {*} entity = req.body
 * @returns {Promise}
 */
async function update(query, entity) {
    const { MenuAccessModel } = ctx;

    const isExists = MenuAccessModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Menu Access Not Found!!!');
    }

    const menuaccess = await MenuAccessModel.update(query, entity);

    return menuaccess;
}

/**
 * Delete or Remove Menu Access
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function remove(query) {
    const { MenuAccessModel } = ctx;

    const isExists = await MenuModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Menuaccess Not Found!!!');
    }

    const menuaccess = await MenuAccessModel.remove(query);

    if (menuaccess) {
        return { data: "Menu access has been deleted" };
    }
}

module.exports = {
    create,
    getAll,
    getOne,
    update,
    remove
}