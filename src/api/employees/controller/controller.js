
/**
 * Create Employee
 * 
 * @param {*} entity  = req.body
 * @returns {Promise}
 */
async function create(entity) {
    const { EmployeeModel } = ctx;

    const employee = await EmployeeModel.create(entity);

    return employee;

}

/**
 * Get All Employee with entity 
 * 
 * @param {*} query = {}
 * @returns {Promise}
 */
async function getAll(query) {
    const { EmployeeModel } = ctx;

    const employees = await EmployeeModel.getAll(query);

    return employees;
}

/**
 * Get One Employee with entity
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function getOne(query) {
    const { EmployeeModel } = ctx;

    const employee = await EmployeeModel.getOne(query);

    if (!employee) {
        return new errors.NotFoundError('Employee Not Found!!!');
    }

    return employee;
}

/**
 * Update Employee 
 * 
 * @param {*} query = { _id: req.params.id}
 * @param {*} entity = req.body
 * @returns {Promise}
 */
async function update(query, entity) {
    const { EmployeeModel } = ctx;

    const isExists = await EmployeeModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Employee Not Found!!!');
    }

    const employee = await EmployeeModel.update(query, entity);

    return employee;
}

/**
 * Delete Employee
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function remove(query) {
    const { EmployeeModel } = ctx;

    const isExists = await EmployeeModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Employee Not Found!!!');
    }

    const employee = await EmployeeModel.remove(query);

    if (employee) {
        return { data: 'Employee has been deleted successfully' };
    }

}


module.exports = {
    create,
    getAll,
    getOne,
    update,
    remove
}