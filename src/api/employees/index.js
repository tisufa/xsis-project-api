
const Routes = require('./routes');
const EmployeeModel = require('./model/model');
const Employee = require('./controller/controller');

module.exports = {
    name: 'Employees',
    start: async (server) => {

        Object.assign(ctx, { Employee, EmployeeModel });
        server.route(Routes);
    }
}
