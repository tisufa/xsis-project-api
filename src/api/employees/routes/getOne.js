
exports.route = {
    method: 'GET',
    path: '/api/employee/:id',
    auth: true,
    async handler(req, res) {
        const { Employee } = ctx;
        const query = { _id: req.params.id };
        const employee = await Employee.getOne(query);
        res.send(200, employee);
    }
}