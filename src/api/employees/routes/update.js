
exports.route = {
    method: 'PUT',
    path: '/api/employee/:id',
    auth: true,
    async handler(req, res) {
        const { Employee } = ctx;
        const query = { _id: req.params.id };
        const entity = req.body;
        const employee = await Employee.update(query, entity);
        res.send(200, employee);
    }
}