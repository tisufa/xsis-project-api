
exports.route = {
    method: 'GET',
    path: '/api/employees',
    auth: true,
    async handler(req, res) {
        const { Employee } = ctx;
        const employees = await Employee.getAll({})
        res.send(200, employees);
    }
}