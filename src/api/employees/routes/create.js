
exports.route = {
    method: 'POST',
    path: '/api/employee',
    auth: true,
    async handler(req, res) {
        const { Employee } = ctx;
        const entity = req.body;
        const employee = await Employee.create(entity);
        res.send(200, employee);
    }
}