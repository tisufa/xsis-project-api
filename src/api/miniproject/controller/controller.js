
/**
 * Create Miniproject
 * 
 * @param {*} entity  = req.body
 * @returns {Promise}
 */
async function create(entity) {
    const { MiniprojectModel } = ctx;

    const miniproject = await MiniprojectModel.create(entity);

    return miniproject;
}

/**
 * Get All Miniproject with entity 
 * 
 * @param {*} query = {}
 * @returns {Promise}
 */
async function getAll(query) {
    const { MiniprojectModel } = ctx;

    const miniprojects = await MiniprojectModel.getAll(query);

    return miniprojects;
}

/**
 * Get One Miniproject with entity
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function getOne(query) {
    const { MiniprojectModel } = ctx;

    const miniproject = await MiniprojectModel.getOne(query);

    if (!miniproject) {
        return new errors.NotFoundError('Miniproject Not Found!!!');
    }

    return miniproject;
}

/**
 * Update Miniproject 
 * 
 * @param {*} query = { _id: req.params.id}
 * @param {*} entity = req.body
 * @returns {Promise}
 */
async function update(query, entity) {
    const { MiniprojectModel } = ctx;

    const isExists = await MiniprojectModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Miniproject Not Found!!!');
    }

    const miniproject = await MiniprojectModel.update(query, entity);

    return miniproject;
}

/**
 * Delete Miniproject
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function remove(query) {
    const { MiniprojectModel } = ctx;

    const isExists = await MiniprojectModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Miniproject Not Found!!!');
    }

    const miniproject = await MiniprojectModel.remove(query);

    if (miniproject) {
        return { data: 'Miniproject has been deleted successfully' };
    }
}

module.exports = {
    create,
    getAll,
    getOne,
    update,
    remove
}