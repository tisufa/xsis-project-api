
exports.route = {
    method: 'GET',
    path: '/api/miniproject/:id',
    auth: true,
    async handler(req, res) {
        const { Miniproject } = ctx;
        const query = { _id: req.params.id };
        const miniproject = await Miniproject.getOne(query);
        res.send(200, miniproject);
    }
}