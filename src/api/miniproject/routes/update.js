
exports.route = {
    method: 'PUT',
    path: '/api/miniproject/:id',
    auth: true,
    async handler(req, res) {
        const { MiniprojectModel } = ctx;
        const query = { _id: req.params.id };
        const entity = req.body;
        const miniproject = await MiniprojectModel.update(query, entity);
        res.send(200, miniproject);
    }
}