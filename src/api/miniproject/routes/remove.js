
exports.route = {
    method: 'DEL',
    path: '/api/miniproject/:id',
    auth: true,
    async handler(req, res) {
        const { Miniproject } = ctx;
        const query = { _id: req.params.id }
        const miniproject = await Miniproject.remove(query);
        res.send(200, miniproject);
    }
}