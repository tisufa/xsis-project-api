
exports.route = {
    method: 'GET',
    path: '/api/miniprojects',
    auth: true,
    async handler(req, res) {
        const { Miniproject } = ctx;
        const miniprojects = await Miniproject.getAll({})
        res.send(200, miniprojects);
    }
}