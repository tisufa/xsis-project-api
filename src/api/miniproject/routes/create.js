
exports.route = {
    method: 'POST',
    path: '/api/miniproject',
    auth: true,
    async handler(req, res) {
        const { Miniproject } = ctx;
        const entity = req.body;
        const miniproject = await Miniproject.create(entity);
        res.send(200, miniproject);
    }
}