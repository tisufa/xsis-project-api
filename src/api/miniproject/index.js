
const Routes = require('./routes');
const MiniprojectModel = require('./model/model');
const Miniproject = require('./controller/controller');

module.exports = {
    name: 'Miniproject',
    start: async (server) => {
        
        Object.assign(ctx, { Miniproject, MiniprojectModel });
        server.route(Routes);
    }
}
