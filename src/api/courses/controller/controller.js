
/**
 * Create Courses
 * 
 * @param {*} entity = req.body
 * @return {Promise}
 */
async function create(entity) {
    const { CourseModel } = ctx;

    const courses = await CourseModel.create(entity);

    return courses;
}

/**
 * Get All Courses
 * @param {*} query = {}
 * @return {Promise}
 */
async function getAll(query) {
    const { CourseModel } = ctx;

    const courses = await CourseModel.getAll(query);

    return courses;
}

/**
 * Get One
 * 
 * @param {*} query = { _id: req.params.id}
 * @return {Promise}
 */
async function getOne(query) {
    const { CourseModel } = ctx;

    const course = await CourseModel.getOne(query);

    if (!course) {
        return new errors.NotFoundError('Course Not Found!!!');
    }

    return course;
}

/**
 * Update Course
 * 
 * @param {*} query = { _id: req.params.id }
 * @param {*} entity = req.body
 * @return {Promise}
 */
async function update(query, entity) {
    const { CourseModel } = ctx;

    const isExists = await CourseModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Course Not Found!!!');
    }

    const course = await CourseModel.update(query, entity);

    return course;
}

/**
 * Delete Course
 * 
 * @param {*} query = { _id: req.params.id}
 * @return {Promise}
 */
async function remove(query) {
    const { CourseModel } = ctx;

    const isExists = await CourseModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Course Not Found!!!');
    }

    const course = await CourseModel.remove(query);

    if (course) {
        return { data: 'Course has been deleted successfully' }
    }
}


module.exports = {
    create,
    getAll,
    getOne,
    update,
    remove
}