
exports.route = {
    method: 'PUT',
    path: '/api/course/:id',
    auth: true,
    async handler(req, res) {
        const { Course } = ctx;
        const query = { _id: req.params.id },
              entity = req.body;
        const course = await Course.update(query, entity);
        res.send(200, course);
    }
}