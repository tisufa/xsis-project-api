
exports.route = {
    method: 'GET',
    path: '/api/courses',
    auth: true, 
    async handler(req, res) {
        const { Course } = ctx;
        const courses = await Course.getAll({});
        res.send(200, courses);
    }
}