
exports.route = {
    method: 'DEL',
    path: '/api/course/:id',
    auth: true,
    async handler(req, res) {
        const { Course } = ctx;
        const query = { _id: req.params.id }
        const course = await Course.remove(query);
        res.send(200, course);
    }
}