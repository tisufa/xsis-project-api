
exports.route = {
    method: 'POST',
    path: '/api/course',
    auth: true,
    async handler(req, res) {
        const { Course } = ctx;
        const entity = req.body;
        const course = await Course.create(entity);
        res.send(200, course);
    }
}