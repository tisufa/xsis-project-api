
const Routes = require('./routes');
const CourseModel = require('./model/model');
const Course = require('./controller/controller');

module.exports = {
    name: 'Courses',
    start: async (server) => {

        Object.assign(ctx, { Course, CourseModel });
        server.route(Routes);
    }
}
