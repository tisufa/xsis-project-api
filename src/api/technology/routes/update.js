
exports.route = {
    method: 'PUT',
    path: '/api/technology/:id',
    auth: true,
    async handler(req, res) {
        const { Technology } = ctx;
        const query = { _id: req.params.id };
        const entity = req.body;
        const technology = await Technology.update(query, entity);
        res.send(200, technology);
    }
}