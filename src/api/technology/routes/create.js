
exports.route = {
    method: 'POST',
    path: '/api/technology',
    auth: true,
    async handler(req, res) {
        const { Technology } = ctx;
        const entity = req.body;
        const technology = await Technology.create(entity);
        res.send(200, technology);
    }
}