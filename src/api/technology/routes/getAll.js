
exports.route = {
    method: 'GET',
    path: '/api/technologies',
    auth: true,
    async handler(req, res) {
        const { Technology } = ctx;
        const technologies = await Technology.getAll({})
        res.send(200, technologies);
    }
}