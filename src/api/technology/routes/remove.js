
exports.route = {
    method: 'DEL',
    path: '/api/technology/:id',
    auth: true,
    async handler(req, res) {
        const { Technology } = ctx;
        const query = { _id: req.params.id }
        const technology = await Technology.remove(query);
        res.send(200, technology);
    }
}