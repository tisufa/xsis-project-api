
const Routes = require('./routes');
const TechnologyModel = require('./model/model');
const Technology = require('./controller/controller');

module.exports = {
    name: 'Technologies',
    start: async (server) => {

        Object.assign(ctx, { Technology, TechnologyModel });
        server.route(Routes);
    }
}
