
/**
 * Create Technology
 * 
 * @param {*} entity  = req.body
 * @returns {Promise}
 */
async function create(entity) {
    const { TechnologyModel } = ctx;

    const technology = await TechnologyModel.create(entity);

    return technology;
}

/**
 * Get All Technology with entity 
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function getAll(query) {
    const { TechnologyModel } = ctx;

    const technology = await TechnologyModel.getAll(query);

    return technology;
}

/**
 * Get One Technology with entity
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function getOne(query) {
    const { TechnologyModel } = ctx;

    const technology = await TechnologyModel.getOne(query);

    if (!technology) {
        return new errors.NotFoundError('Technology Not Found!!!');
    }

    return technology;
}

/**
 * Update Technology 
 * 
 * @param {*} query = { _id: req.params.id}
 * @param {*} entity = req.body
 * @returns {Promise}
 */
async function update(query, entity) {
    const { TechnologyModel } = ctx;

    const isExists = await TechnologyModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Technology Not Found!!!');
    }

    const technology = await TechnologyModel.update(query, entity);

    return technology;
}

/**
 * Delete Technology
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function remove(query) {
    const { TechnologyModel } = ctx;

    const isExists = await TechnologyModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('Technology Not Found!!!');
    }

    const technology = await TechnologyModel.remove(query);

    if (technology) {
        return { data: 'Technology has been deleted successfully' };
    }

}

module.exports = {
    create,
    getAll,
    getOne,
    update,
    remove
}