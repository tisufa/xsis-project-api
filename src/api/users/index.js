
const Routes = require('./routes');
const UserModel = require('./model/model');
const User = require('./controller/controller');

module.exports = {
    name: 'Users',
    start: async (server) => {

        Object.assign(ctx, { User, UserModel });
        server.route(Routes);
    }
}
