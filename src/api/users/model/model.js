
const Model = require(files.toCommon('Model'));

class UserModel extends Model {
    constructor(Collection) {
        super(Collection);
    }

    async addFile(files){
        const collection = await this.Collection('images');
        Array.isArray(files) ? files : files = [files];
        this.result = collection.insertMany(files)
            .then(result => {
                return result.ops.length > 1 ? result.ops : result.ops[0];
            }).catch(err => {
                throw 'Message: ' + err;
            });
             
            return this.result;
    }
}

module.exports = new UserModel('users');