
exports.route = {
    method: 'GET',
    path: '/api/user/:id',
    auth: true,
    async handler(req, res) {
        const { User } = ctx;
        const query = {_id: req.params.id};
        const user = await User.getOne(query);
        res.send(200, user);
    }
}