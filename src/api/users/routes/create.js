
exports.route = {
    method: 'POST',
    path: '/api/user',
    auth: true,
    async handler(req, res) {
        const { User } = ctx;
        const entity = req.body;
        const files = req.files;
        const user = await User.create(entity, files);
        res.send(200, user);
    }
}