
exports.route = {
    method: 'PUT',
    path: '/api/user/:id',
    auth: true,
    async handler(req, res) {
        const { User } = ctx;
        const query = { _id: req.params.id };
        const entity = req.body;
        const user = await User.update(query, entity);
        res.send(200, user);
    }
}