
exports.route = {
    method: 'DEL',
    path: '/api/user/:id',
    auth: true,
    async handler(req, res) {
        const { User } = ctx;
        const query = { _id: req.params.id }
        const user = await User.remove(query);
        res.send(200, user);
    }
}