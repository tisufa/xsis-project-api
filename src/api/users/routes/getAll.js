
exports.route = {
    method: 'GET',
    path: '/api/users',
    auth: true,
    async handler(req, res) {
        const { User } = ctx;
        const users = await User.getAll({});
        res.send(200, users);
    }
}