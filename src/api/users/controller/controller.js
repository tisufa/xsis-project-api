
/**
 * Create User
 * 
 * @param {*} entity  = req.body
 * @returns {Promise}
 */
async function create(entity, files) {
    const { UserModel, auth } = ctx;

    const imageId = await UserModel.addFile(files).then(() => {
        return UserModel.lastId('_id');
    });

    // Add image _id to entity
    entity[0]['id_image'] = imageId;

    const query = {
        username: entity[0].username
    }

    const isExists = await UserModel.isExists(query);

    if (isExists) {
        const message = {
            message: 'Username is Exists'
        }

        return message;
    }

    if(entity[0].password) {
        entity[0].password = await auth.encode(entity[0].password);
    }

    const user = await UserModel.create(entity);

    return user;
}

/**
 * Get All User with entity 
 * 
 * @param {*} query = {}
 * @returns {Promise}
 */
async function getAll(query) {
    const { UserModel } = ctx;

    const users = await UserModel.getAll(query);

    return users;
}

/**
 * Get One User with entity
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function getOne(query) {
    const { UserModel } = ctx;

    const user = await UserModel.getOne(query);

    if (!user) {
        return new errors.NotFoundError('User Not Found!!!');
    }

    return user;
}

/**
 * Update User 
 * 
 * @param {*} query = { _id: req.params.id}
 * @param {*} entity = req.body
 * @returns {Promise}
 */
async function update(query, entity) {
    const { UserModel } = ctx;

    const isExists = await UserModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('User Not Found!!!');
    }

    const user = await UserModel.update(query, entity);

    return user;
}

/**
 * Delete User
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function remove(query) {
    const { UserModel } = ctx;

    const isExists = await UserModel.isExists(query);

    if (!isExists) {
        return new errors.NotFoundError('User Not Found!!!');
    }

    const user = await UserModel.remove(query);

    if (user) {
        return { data: 'User has been deleted successfully' };
    }
}

module.exports = {
    create,
    getAll,
    getOne,
    update,
    remove
}