
exports.route = {
    method: 'PUT',
    path: '/api/softskill/:id',
    auth: true,
    async handler(req, res) {
        const { Softskill } = ctx;
        const query = { _id: req.params.id };
        const entity = req.body;
        const softskill = await Softskill.update(query, entity);
        res.send(200, softskill);
    }
}