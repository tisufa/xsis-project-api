
exports.route = {
    method: 'GET',
    path: '/api/softskill/:id',
    auth: true,
    async handler(req, res) {
        const { Softskill } = ctx;
        const query = { _id: req.params.id };
        const softskill = await Softskill.getOne(query);
        res.send(200, softskill);
    }
}