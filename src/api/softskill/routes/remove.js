
exports.route = {
    method: 'DEL',
    path: '/api/softskill/:id',
    auth: true,
    async handler(req, res) {
        const { Softskill } = ctx;
        const query = { _id: req.params.id }
        const softskill = await Softskill.remove(query);
        res.send(200, softskill);
    }
}