
exports.route = {
    method: 'GET',
    path: '/api/softskills',
    auth: true,
    async handler(req, res) {
        const { Softskill } = ctx;
        const softskills = await Softskill.getAll({})
        res.send(200, softskills);
    }
}