
exports.route = {
    method: 'POST',
    path: '/api/softskill',
    auth: true,
    async handler(req, res) {
        const { Softskill } = ctx;
        const entity = req.body;
        const softskill = await Softskill.create(entity);
        res.send(200, softskill);
    }
}