
const Routes = require('./routes');
const SoftskillModel = require('./model/model');
const Softskill = require('./controller/controller');

module.exports = {
    name: 'Softskill',
    start: async (server) => {

        Object.assign(ctx, { Softskill, SoftskillModel });
        server.route(Routes);
    }
}
