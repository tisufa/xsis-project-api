
/**
 * Create Softskill
 * 
 * @param {*} entity  = req.body
 * @returns {Promise}
 */
async function create(entity) {
    const { SoftskillModel } = ctx;

    const softskill = await SoftskillModel.create(entity);

    return softskill;
}

/**
 * Get All Softskill with entity 
 * 
 * @param {*} query = {}
 * @returns {Promise}
 */
async function getAll(query) {
    const { SoftskillModel } = ctx;

    const softskills = await SoftskillModel.getAll(query);
    
    return softskills;
}

/**
 * Get One Softskill with entity
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function getOne(query) {
   const { SoftskillModel } = ctx;

   const softskill = await SoftskillModel.getOne(query);

   if(!softskill){
       return new errors.NotFoundError('Softskill Not Found!!!');
   }

   return softskill;
}

/**
 * Update Softskill 
 * 
 * @param {*} query = { _id: req.params.id}
 * @param {*} entity = req.body
 * @returns {Promise}
 */
async function update(query, entity) {
    const { SoftskillModel } = ctx;

    const isExists = await SoftskillModel.isExists(query);

    if(!isExists){
        return new errors.NotFoundError('Softskill Not Found!!!');
    }

    const softskill = await SoftskillModel.update(query, entity);

    return softskill;
}

/**
 * Delete Softskill
 * 
 * @param {*} query = { _id: req.params.id }
 * @returns {Promise}
 */
async function remove(query) {
    const { SoftskillModel } = ctx;

    const isExists = await SoftskillModel.isExists(query);

    if(!isExists){
        return new errors.NotFoundError('Softskill Not Found!!!');
    }

    const softskill = await SoftskillModel.remove(query);

    if(softskill){
        return { data: 'Softskill has been deleted successfully'};
    }
}

module.exports = {
    create,
    getAll,
    getOne,
    update,
    remove
}