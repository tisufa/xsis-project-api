
/**
 * User Login
 * 
 * @param {*} entity 
 * @returns {Array}
 */
async function login(entity) {
    const { AuthModel, auth } = ctx;

    const query = {
        username: entity.username
    }

    const user = await AuthModel.getOne(query);

    if (!user) {
        return new errors.NotFoundError('User Not Found!!!');
    }

    const checked = await auth.decode(entity.password.toString(), user.password.toString());

    if (!checked) {
        return { message: 'Username or password failed!' }
    }

    const token = jwt.sign({ user }, 'secretkey');

    return { user: user.username, token };
}

module.exports = {
    login
}