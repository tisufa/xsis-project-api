
exports.route = {
    method: 'POST',
    path: '/api/auth/login',
    async handler(req, res) {
        const { Auth } = ctx;
        const entity = {
            username: req.body[0].username,
            password: req.body[0].password
        };
        
        const login = await Auth.login(entity);
        res.send(200, login);
    }
}