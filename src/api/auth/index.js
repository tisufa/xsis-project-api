
const Routes = require('./routes');
const AuthModel = require('./model/model');
const Auth = require('./controller/controller');

module.exports = {
    name: 'Auth',
    start: async (server) => {

        Object.assign(ctx, { Auth, AuthModel });
        server.route(Routes);
    }
}
