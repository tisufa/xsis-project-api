
const Model = require(files.toCommon('Model'));

class AuthModel extends Model {
    constructor(Collection) {
        super(Collection);
    };

    /**
     * Create Login for AuthModel
     * 
     * @param {*} entity
     * @returns {Array}
     */
    async login(entity){
        const collection = await this.collection;
        return collection.find(entity).toArray()
               .then(user => {
                   return user[0];
               }).catch(err => {
                   throw 'Message: ' + err;
               });
    };
}

module.exports = new AuthModel('users');