
class Vendor {

    /**
     * Register all plugins and make it global
     */
    start(){
        global.errors = require('restify-errors');
        global.bcrypt = require('bcrypt');
        global.jwt = require('jsonwebtoken');
    }
}

module.exports = new Vendor();