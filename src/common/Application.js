const fs = require('fs');
const vendor = require('./vendor/vendor');
const _commonCore = Symbol('_commonCore'),
    _helperCore = Symbol('_helperCore'),
    _startHelperCore = Symbol('_startHelperCore'),
    _startCommonCore = Symbol('_startCommonCore');

class Application {

    /**
     * Start Application
     */
    start() {
        vendor.start();
        this[_startHelperCore]();
        this[_startCommonCore]();
    }

    /**
     * Register Common Core
     * @returns {Array}
     */
    [_commonCore]() {
        return ['Database', 'Routes', 'Auth']
    }

    /**
     * Get All Helpers from helper directory
     * @returns {Array}
     */
    [_helperCore]() {
        return fs.readdirSync(files.toCommon('/helper'));
    }


    /**
     * Start Helper Core
     */
    [_startHelperCore]() {
        this[_helperCore]().map(core => {

            const helper = require(files.toCommon('helper/') + core);

            const helperType = typeof helper;

            if (helperType === 'function') {
                require(files.toCommon('helper/') + core)();
            } else if (helperType === 'object') {
                const Helper = { ...helper };
                Object.assign(ctx, { Helper });
            } else {
                require(files.toCommon('helper/') + core)
            }
        })

    }

    /**
     * Start Common Core
     */
    [_startCommonCore]() {
        this[_commonCore]().map(core => {
            
            if (core === 'Auth') {
                const auth = require(files.toCommon(core));
                Object.assign(ctx, { auth });
            } else {
                require(files.toCommon(core));
            }
        });

    }

}

module.exports = new Application();