
class Auth {

    /**
     * Hash password with bcrypt
     * 
     * @param {*|String} password 
     * @returns {String}
     */
    async encode(password) {
        const saltround = 10;
        return bcrypt.hash(password, saltround).then(hash => {
            return hash
        }).catch(err => {
            throw 'Message: ' + err;
        })
    }

    /**
     * Compare password with password hash with bcrypt
     * 
     * @param {*|String} password 
     * @param {*|String} hash 
     * @returns {Boolean}
     */
    async decode(password, hash) {
        return bcrypt.compare(password, hash).then(response => {
            return response;
        }).catch(err => {
            throw 'Message: ' + err;
        })

    }
}

module.exports = new Auth();