
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const config = require(files.to('/config/config'));

// Create Private Method
const _connect = Symbol('_connect'),
      _db = Symbol('_db'),
      _collection = Symbol('_collection');

class Database {
    constructor(collection) {
        this.collection = collection;
        this.connection = this[_connect]();
        this.dbo = this[_db]();
        this.collection = this[_collection](collection);
    }

    /**
     * Connect to database url
     */
    async [_connect]() {
        return MongoClient.connect(config.db.uri, { useNewUrlParser: true });
    }

    /**
     * Set database name
     */
    async [_db]() {
        return await this.connection.then(client => {
            return client.db(config.db.name);
        }).catch(err => {
            assert.equal(null, err);
        });

    }

    /**
     * Set collection name
     */
    async [_collection](collection) {
        const db = await this.dbo;

        collection ? collection : collection = this.collection;

        return db.collection(collection);
    }
}

module.exports = Database;