
const ObjectID = require('mongodb').ObjectID;
const { server, Helper } = ctx;

module.exports = () => {

    /**
     * Set server middleware for helper
     */
    server['middleware'] = (req, res, next) => {
        if (req.params.id) {
            req.params.id = ObjectID(req.params.id)
        };

        if (req.method === 'POST' || req.method === 'PUT') {
            Helper.createdAndUpdatedDate(req);
            Helper.setIsDeleted(req);
            Helper.convertToObjectId(req);
            Helper.setStringToDate(req);
            Helper.uploads(req);
        }

        next();
    }

    /**
     * Set server response
     */
    server['response'] = (result, message) => {
        const { req, res } = hand;

        if (req.method === 'GET') {
            res.status(200);
            res.json({
                success: true,
                error_code: null,
                message: message,
                data: result
            });
        }
    }

    /**
     * Set server verifytoken for middleware
     */
    server['verifyToken'] = (req, res, next) => {
        const bearerHeader = req.headers['authorization'];
        if (typeof bearerHeader !== 'undefined') {

            const bearer = bearerHeader.split(' ');

            const bearerToken = bearer[1];

            req.token = bearerToken;

            next();

        } else {
            res.send(403, { message: 'Forbidden!!!' })
        }
    }

    /**
     * Set server route = server.route(rooute)
     */
    server['route'] = (routes) => {

        if (routes.method && routes.path && routes.handler) {
            routes = Object.assign({}, { routes });
            route = Object.assign({}, { routes });
            routes = Object.assign({}, route);
        }

        const keys = Object.keys(routes);

        keys.map(key => {
            const route = routes[key];
            const routeKey = Object.keys(route)[0];
            const method = (route[routeKey].method).toLocaleLowerCase(),
                path = (route[routeKey].path).toLocaleLowerCase();

            if (route[routeKey].auth) {
                server[method](path, server.middleware, server.verifyToken, (req, res, next) => {
                    global.hand = Object.assign({}, { req, res });
                    route[routeKey].handler(req, res);
                })
            } else {
                server[method](path, server.middleware, (req, res, next) => {
                    global.hand = Object.assign({}, { req, res });
                    route[routeKey].handler(req, res);
                })
            }
        })
    }
}