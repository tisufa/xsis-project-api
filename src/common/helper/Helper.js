/**
 * API Helper
 */

const ObjectID = require('mongodb').ObjectID;
const Upload = require('../Upload');

function Helper() {

    /**
     * set created_date and updated_date to req.body
     * 
     * @param {Object} req
     */
    this.createdAndUpdatedDate = (req) => {
        const method = req.method,
            entity = Array.isArray(req.body) ? req.body : req.body = [req.body],
            date = new Date();

        if (method === 'POST' && req.body[0] !== undefined) {
            console.log('hahaha');
            for (let key in entity) {
                ent = entity[key]
                ent['created_date'] = date;
                ent['updated_date'] = date;
            }
        } else if (method === 'PUT' && req.body[0] !== undefined) {
            for (let key in entity) {
                ent = entity[key];
                ent['updated_date'] = date;
            }

            req.body.length > 1 ? req.body : req.body = req.body[0];
        }
    }

    /**
     * Convert everityng _id to ObjectID
     * 
     * @param {Object} req
     */
    this.convertToObjectId = (req) => {
        const entity = req.body;
        const name = ['created_by', 'updated_by', 'id_technology', 'id_employee', 'id_role', 'id_course',
            'id_batch', 'id_miniproject', 'id_hardskill', 'id_softskill', 'id_course_batch',
            'id_menu'];

        for (let idx in entity) {
            const data = entity[idx];
            for (let key in data) {
                if (name.includes(key)) {
                    entity[idx][key] = ObjectID(data[key]);
                }
            }
        }
    }

    /**
     * Set is_deleted = 0 to req.body
     * 
     * @param {Object} req
     */
    this.setIsDeleted = (req) => {
        if (req.method === 'POST' && req.body[0] !== undefined) {
            const entity = Array.isArray(req.body) ? req.body : req.body = [req.body];
            for (let key in entity) {
                ent = entity[key];
                ent['is_deleted'] = 0;
            }
        }
    }

    /**
     * Set Date Format
     * 
     * @param {Date} date
     * @returns {String}
     */
    this.setDateFormat = (date) => {
        const arrMonth = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
        let day = date.getDate();
        let month = date.getMonth();
        let year = date.getFullYear();
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let seconds = date.getSeconds();

        return day + ' ' + arrMonth[month] + ' ' + year + '; ' + hours + ':' + minutes + ':' + seconds
    }

    /**
     * Set string to date to req.body
     * 
     * @param {Object} req
     * @returns {Array}
     */
    this.setStringToDate = (req) => {
        const arrDate = ['start_date', 'end_date'];
        const entity = req.body;
        const errDate = [];
        entity.map(data => {
            for (let idx in data) {
                if (arrDate.includes(idx)) {
                    let date = new Date(data[idx]);

                    if (date == 'Invalid Date') {
                        date = 'Invalid Date Format';
                        errDate.push(data);
                    }
                    data[idx] = date;
                }
            }
        });
    }

    /**
     * Upload file to API if req.files != 0
     * 
     * @param {Object} req
     * @returns {Array}
     */
    this.uploads = (req) => {
        if(req.files && req.body.length === 1){
            req.files = Upload.file(req.files).to('images');
        }

    }
}

module.exports = new Helper();