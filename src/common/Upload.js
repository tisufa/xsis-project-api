
const fs = require('fs');

class Upload {

    file(file) {
        this.files = file[''];
        return this;
    }

    to(path, name) {

        const fileData = fs.readFileSync(this.files.path);

        if (!(fs.existsSync(files.toUploads(path)))) {
            fs.mkdirSync(files.toUploads(path));
        }

        name ? name = name + this.files.name.slice(this.files.name.lastIndexOf('.')) : name = new Date().getTime() + '-' + this.files.name;

        path ? path : path = 'images';

        if (fileData) {
            const newPath = files.toUploads(path) + '\\' + name;

            fs.writeFileSync(newPath, fileData);
            return {
                path: path,
                name: name,
                size: this.files.size,
                type: this.files.type,
                extension: this.files.name.slice(this.files.name.lastIndexOf('.'))
            }
        }
    }

    name() {

        this.name = this.files.name;

        return this;
    }

    type() {
        this.type = this.files.type;
        return this;
    }

    size() {
        this.size = this.files.size;
        return this;
    }

    path() {
        this.paths = this.files.path;
        return this;
    }

    extension() {
        const name = this.name();
        this.extension = name.slice(name.lastIndexOf('.'));

        return this;
    }



}

module.exports = new Upload();