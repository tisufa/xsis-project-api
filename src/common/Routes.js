
const fs = require('fs');
const { server } = ctx;

class Routes {

    /**
     * Routes constructor
     */
    constructor() {
        this.start();
    }

    /**
     * Get all routes from API with require and start route
     */
    start() {
        const routes = fs.readdirSync(files.toApi());
        routes.map(route => {
            require(files.toApi(route)).start(server);
        });
    }
}

module.exports = new Routes();