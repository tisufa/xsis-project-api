
const path = require('path');
const fs = require('fs');

const replacePath = Symbol('replacePath');

class Files {

    /**
     * Get API Base Directory
     */
    baseDir() {
        return path.resolve();
    }

    /**
     * To another path with path
     * 
     * @param {*|String} path 
     * @returns {String}
     */
    to(path) {
        return this.baseDir() + this[replacePath](path);
    }

    /**
     * To api directory
     * 
     * @param {*|String} path 
     * @returns {String}
     */
    toApi(path) {

        const basePath = this.baseDir() + '\\src\\api\\';

        return path ? basePath + this[replacePath](path) : basePath;
    }

    /**
     * To common directory
     * 
     * @param {*|String} path 
     * @returns {String}
     */
    toCommon(path) {

        const basePath = this.baseDir() + '\\src\\common\\';

        return path ? basePath + this[replacePath](path) : basePath;
    }

    /**
     * To uploads direcotry
     * 
     * @param {*|String} path
     * @returns {String} 
     */
    toUploads(path) {
        const basePath = this.baseDir() + '\\public\\uploads\\';

        return path ? basePath + this[replacePath](path) : basePath;
    }

    /**
     * Check if directory of files exists
     * 
     * @param {*|String} path
     * @returns {Boolean} 
     */
    exists(path) {
        return fs.existsSync(path);
    }

    /**
     * Create directory if direcotry does not exists
     * 
     * @param {*|String} path
     * @returns {String} 
     */
    create(path) {
        if (!this.exists(path)) {
            fs.mkdirSync(path)
        }
    }

    /**
     * Replace string / with \\
     * 
     * @param {*|String} path 
     * @returns {String}
     */
    [replacePath](path) {
        return path.replace('/', '\\')
    }
}

module.exports = new Files();