const DB = require(files.toCommon('Database'));

class Model extends DB {
    constructor(collection) {
        super(collection);
    }

    /**
     * Create entity
     * 
     * @param {*} entity = { req.body }
     * @return {Promise}
     */
    async create(entity) {
        const collection = await this.collection;
        Array.isArray(entity) ? entity : entity = [entity];
        this.result = collection.insertMany(entity)
            .then(result => {
                return result.ops.length > 1 ? result.ops : result.ops[0];
            }).catch(err => {
                throw 'Message: ' + err;
            });

        return this.result;
    }

    /**
     * Get All entity
     * 
     * @param {*} entity = {} || some query
     * @return {Promise}
     */
    async getAll(entity) {
        const collection = await this.collection;
        entity ? entity : {};
        return collection.find(entity).toArray()
            .then(result => {
                return result;
            }).catch(err => {
                throw 'Message: ' + err;
            });
    }

    /**
     * Get One entity
     * 
     * @param {*} entity = { _id: req.params.id}
     * @return {Promise}
     */
    async getOne(entity) {
        const collection = await this.collection;
        return collection.find(entity).toArray()
            .then(result => {
                return result[0];
            }).catch(err => {
                throw 'Message: ' + err;
            });
    }

    /**
     * Update entity
     * 
     * @param {*} query = { _id: req.params.id }
     * @param {*} entity = { req.body }
     * @return {Promise}
     */
    async update(query, entity) {
        const collection = await this.collection
        return collection.findOneAndUpdate(query, { $set: entity }, { returnOriginal: false })
            .then(result => {
                return result.value;
            }).catch(err => {
                throw 'Message: ' + err;
            })
    }

    /**
     * Delete entity
     * 
     * @param {*} query = { _id: req.params.id}
     * @return {Promise}
     */
    async remove(query) {
        const collection = await this.collection
        return collection.findOneAndDelete(query)
            .then(result => {
                return true;
            })
            .catch(err => {
                throw 'Message: ' + err;
            });
    }

    /**
     * Check if Exists
     * 
     * @param {*} query = { _id: req.params.id } || some query
     * @return {Promise}
     */
    async isExists(query) {
        const collection = await this.collection
        return collection.find(query).toArray()
            .then(result => {
                return result.length > 0 ? true : false;
            }).catch(err => {
                throw 'Message: ' + err;
            })
    }

    /**
     * Get last data from a collection
     * 
     * @param {*} limit = 1,2,dst..
     * @returns {Promise}
     */
    async lastData(limit) {
        const collection = await this.collection
        limit = limit || 1;
        return collection.find({}).sort({ $natural: -1 }).limit(limit).toArray()
            .then(result => {
                return result;
            }).catch(err => {
                throw 'Message: ' + err;
            })
    }

    /**
     * Get last _id or last field data from a collection
     * 
     * @param {*} name = name, _id, created_date
     * @returns {Promise}
     */
    async lastId(name) {
        const result = await this.result;

        return name ? result[name] : result._id;
    }
}

module.exports = Model;